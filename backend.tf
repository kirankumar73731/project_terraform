terraform {
  backend "s3" {
    bucket         = "git-bucket-clearsense"
    key            = "terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
  }
}
